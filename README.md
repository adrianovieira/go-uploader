# Integra uploader

Aplicativo para realizar upload de arquivos em formato especializado.

## Requisitos:

Arquivo a ser enviado para o sistema Integra deve conter a seguinte sintaxe:

- Nome do arquivo: `DD_MM_AAAA_hh_mm.txt`  
  exemplo: `06_12_2019_10_24.txt`

Estrutura do arquivo para upload:

| ID |Nome|Entrada|Duração|Saída| Tipo |
|---|---|---|---|---|---|
|818517_3|PGM CONTRACAPA - PGM 12  BL 03|00:01:00|00:18:27|00:19:27|RPRO|
|56825|VH NEUTRA 2019|00:19:27|00:00:05|00:19:32|RVNH|
|56832|VH CLASSIFICACAO INDICATIVA 14 ANOS|00:19:32|00:00:10|00:19:42|RVNH|
|802674_1|PGM NATALIA  TEMPORADA 2  EP 12  BL 01|00:19:42|00:12:06|00:31:48|RPRO|

Este arquivo dever ser colocado no diretório definido nas configurações.

## Instalação

Este programa é de binário único (1) e pode ser instalado em qualquer diretório
no seu sistema operacional.

(1) Binários:

- Linux: integra-uploader
- Windows: integra-uploader.exe

Acesse a página de lançamentos ([releases](http://www-scm.ebc/producao_distribuicao/integra/integra-uploader/-/releases)) para obter a versão mais recente para um dos
sistemas operacionais acima.

### Configuração

Antes de executar o aplicativo de envio, é necessário que seja configurado com o
arquivo de nome `config.json`. Crie-o no mesmo diretório onde houver sido
instalado o programa.

Este arquivo de configuração deve conter os seguintes padrâmetros:

```json
{
  "apiUrl": "http://localhost:3000",
  "apiUser": "usuario",
  "apiPass": "senha",
  "uploadPath": "./upload",
  "removeUploadedFile": true
}
```

  onde:

  - `apiUrl`: URL com endereço raiz do ambiente da API a ser enviado os dados;
  - `apiUser`: *login* de usuário com permissão para envio da programação;
  - `apiPass`: senha de acesso para o usuário;
  - `uploadPath`: diretório onde serão colocados os arquivos a serem enviados;
  - `removeUploadedFile`: Use `true`/`false` para definir se os arquivos serão
     apagados após serem enviados.
