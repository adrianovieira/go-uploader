package roteiro

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"os"
	"path/filepath"
	"regexp"
)

func _uploadFile(roteiroURLUpload string, uploadFilePath string, bearerToken string) error {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	partHeader := textproto.MIMEHeader{}
	partHeader.Add("Content-Disposition",
		fmt.Sprintf("form-data; name=\"roteiro\"; filename=\"%s\"", uploadFilePath))
	partHeader.Add("Content-Type", "text/plain")

	fileWriter, err := bodyWriter.CreatePart(partHeader)
	if err != nil {
		log.Println("Erro ao tentar criar buffer")
		return err
	}

	file, err := os.OpenFile(uploadFilePath, os.O_RDONLY, 0)
	if err != nil {
		log.Println("Erro ao tentar abrir arquivo de roteiro")
		return err
	}
	defer file.Close()

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		return err
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	// envia arquivo de roteiro
	req, err := http.NewRequest("POST", roteiroURLUpload, bodyBuf)
	if err != nil {
		panic(err)
	}
	req.Header.Set("X-Custom-Header", "integraUploader")
	req.Header.Set("Authorization", "Bearer "+bearerToken)
	req.Header.Set("Content-Type", contentType)

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Panicln(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	if res.StatusCode != 202 { //Arquivo aceito
		err = errors.New("Arquivo não enviado [" + res.Status + "]!")
	}
	log.Println(res.Status)
	log.Println(string(body))
	return err

}

// EnviaRoteiro envio de roteiro via API
/**
* envio de roteiro via API
 */
func EnviaRoteiro(roteiroURLUpload string, uploadFilePath string,
	removeUploadedFile bool, bearerToken string) error {

	// padrão do nome do arquivo = `DDMMAAAA.txt`
	var filePattern = `^([0-9]{2})+([0-9]{2})+([0-9]{4})(\.txt)`
	var count = 0

	fileInfo, err := os.Stat(uploadFilePath)
	if os.IsNotExist(err) {
		log.Fatal("Erro: ", fmt.Sprintf("Diretório ou arquivos (%s) não existem", uploadFilePath))
		return err
	} else if fileInfo.IsDir() {
		files, err := ioutil.ReadDir(uploadFilePath)
		if err != nil {
			log.Fatal("Erro: ", fmt.Sprintf("Ocorreu erro ao tentar ler diretório (%s)", uploadFilePath))
			return err
		}

		if len(files) < 1 {
			err = errors.New("Warning: " + "Arquivos não encontrados (" + uploadFilePath + ")!")
			log.Println(err)
			return err
		}

		// verifica se há pelo menos um arquivo a ser enviado
		for _, file := range files {
			matched, err := regexp.MatchString(filePattern, file.Name())
			if err != nil {
				log.Panicln(err)
			}
			if matched {
				count++
				break
			}
		}

		// caso não haja arquivos a serem enviados para execução da aplicação
		if count == 0 {
			err = errors.New("Warning: " + "Arquivos para envio não encontrados (" + uploadFilePath + ")!")
			log.Println(err)
			return err
		}

		count = 0

		log.Println("Warning:", fmt.Sprintf("Arquivos no diretório (%s) serão enviados", uploadFilePath))
		for _, file := range files {

			matched, err := regexp.MatchString(filePattern, file.Name())
			if err != nil {
				log.Panicln(err)
			}

			if matched {
				filePath := (fmt.Sprintf("%s/%s", uploadFilePath, file.Name()))
				err = _uploadFile(roteiroURLUpload, filePath, bearerToken)
				if err == nil {
					log.Println("Info: ", fmt.Sprintf("Arquivo %s enviado", filePath))
					count++
					if removeUploadedFile {
						os.Remove(filePath)
						log.Println("Info: ", fmt.Sprintf("Arquivo %s removido", filePath))
					}
				}
			}
		}

		if count == 1 {
			log.Println("Info: ", fmt.Sprintf("Foi enviado %d arquivo", count))
		} else if count > 1 {
			log.Println("Info: ", fmt.Sprintf("Foram enviados %d arquivos", count))
		}

	} else {
		matched, err := regexp.MatchString(filePattern, filepath.Base(uploadFilePath))
		if err != nil {
			log.Fatalln("Erro no nome do arquivo", err)
		}

		if matched {
			err = _uploadFile(roteiroURLUpload, uploadFilePath, bearerToken)
			if err == nil {
				log.Println("Info: ", fmt.Sprintf("Arquivo %s enviado", uploadFilePath))
				if removeUploadedFile {
					os.Remove(uploadFilePath)
					log.Println("Info: ", fmt.Sprintf("Arquivo %s removido", uploadFilePath))
				}
			}
		}
	}

	return err
}
