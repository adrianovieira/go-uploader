package main

import (
	"fmt"
	"log"
	"os"

	"./common"
	"./roteiro"
)

/**
 * programa principal para envio do roteiro via API
 */
func main() {
	var uploadPath string

	var config = common.GetConfig("./config.json")

	var args = os.Args[1:]
	if len(args) == 1 {
		uploadPath = args[0]

		_, err := os.Stat(uploadPath)
		if os.IsNotExist(err) {
			log.Fatal("Erro: ", fmt.Sprintf("Diretório ou arquivo (%s) não existem", uploadPath))
		}
	} else {
		uploadPath = config.UPLOADPATH
	}

	// padrão do nome do arquivo = `DDMMAAAA.txt`
	var filePattern = `^([0-9]{2})+([0-9]{2})+([0-9]{4})(\.txt)`

	err := common.TemArquivosEnviar(uploadPath, filePattern)
	if err != nil {
		log.Fatal(err)
	}

	var token = common.GetAccessToken(config)

	roteiro.EnviaRoteiro(config.APIURL+`/roteiros/upload`, uploadPath,
		config.REMOVEUPLOADEDFILE, token.ACCESSTOKEN)

}
