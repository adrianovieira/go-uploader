package common

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"fmt"
	"regexp"
	"errors"
)

// Config estrutura para dados de configurações
type Config struct {
	APIURL             string
	APIUSER            string
	APIPASS            string
	UPLOADPATH         string
	REMOVEUPLOADEDFILE bool
}

// Token para acesso à API
type Token struct {
	ACCESSTOKEN string
}

// GetConfig ...
/**
 * obter configurações da aplicação
 */
func GetConfig(configFile string) Config {
	data, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Println("Erro ao tentar abrir arquivo de configurações", err)
	}

	var config Config
	err = json.Unmarshal(data, &config)
	if err != nil {
		log.Println("Erro ao ler arquivo de configurações", err)
	}

	return config
}

// GetAccessToken ...
/**
 * programa principal para envio do roteiro via API
 */
func GetAccessToken(config Config) Token {

	var jsonStr = []byte(`{"access_key": "` + config.APIUSER +
		`", "secret_key": "` + config.APIPASS + `"}`)

	req, err := http.NewRequest("POST", config.APIURL+`/auths/login`, bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Panicln(err)
	}
	req.Header.Set("X-Custom-Header", "integraUploader")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Panicln(err)
	}
	defer res.Body.Close()

	log.Println("response Status:", res.Status)
	log.Println("response Headers:", res.Header)
	body, _ := ioutil.ReadAll(res.Body)

	var token Token
	err = json.Unmarshal(body, &token)
	if err != nil {
		log.Println("Erro ao obter token", err)
	}

	return token
}

// TemArquivosEnviar ...
/**
 * verifica se o diretório contem arquivos no padrão especificado
 */
func TemArquivosEnviar(uploadFilePath string, filePattern string) error {

	var count = 0

	fileInfo, err := os.Stat(uploadFilePath)
	if os.IsNotExist(err) {
		err = errors.New("Erro: " + fmt.Sprintf("Diretório ou arquivos (%s) não existem", uploadFilePath))
		return err
	} else if fileInfo.IsDir() {
		files, err := ioutil.ReadDir(uploadFilePath)
		if err != nil {
			err = errors.New("Erro: " + fmt.Sprintf("Ocorreu erro ao tentar ler diretório (%s)", uploadFilePath))
			return err
		}

		if len(files) < 1 {
			err = errors.New("Warning: " + "Arquivos não encontrados (" + uploadFilePath + ")!")
			return err
		}

		// verifica se há pelo menos um arquivo a ser enviado
		for _, file := range files {
			matched, err := regexp.MatchString(filePattern, file.Name())
			if err != nil {
				log.Panicln(err)
			}
			if matched {
				count++
				break
			}
		}

		// caso não haja arquivos a serem enviados para execução da aplicação
		if count == 0 {
			err = errors.New("Warning: " + "Arquivos para envio não encontrados (" + uploadFilePath + ")!")
			return err
		}
	}

	return err

}
